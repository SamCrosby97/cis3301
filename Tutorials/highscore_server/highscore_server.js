var http = require('http');

var highscores = [{id: 0, type: 'highscore', attributes: {name: 'Fred', score: 12}}, {id: 1, type: 'highscore', attributes: {name: 'Mark', score: 4}}];
var next_id = 2;

var server = http.createServer(function(request, response) {
  if(request.url == '/highscores') {
      /** Set the response headers to allow access from anywhere **/
      response.setHeader('Content-Type', 'application/json');
      response.setHeader('Access-Control-Allow-Origin', '*');
      response.setHeader('Access-Control-Allow-Headers', 'content-type');
      if(request.method == 'GET') {
          /** Handle GET requests, simply sending back the current content pf the highscores variable **/
          response.write(JSON.stringify({
              data: highscores
          }));
          response.end();
      } else if (request.method == 'POST') {
          /** Handle POST requests, adding data to the highscores variable **/
          data = [];
          request.on('data', function(chunk) {
              /** Handle POST submissions is a bit more complex, as data is uploaded in "chunks", which are temporarily stored in the data variable **/
              data.push(chunk);
          });
          request.on('end', function() {
              /** When all data has been uploaded, add it to the highscores variable**/
              var highscore = JSON.parse(Buffer.concat(data).toString());
              highscore.data.id = next_id;
              next_id = next_id + 1;
              highscores.push(highscore.data);
              /**Sort the highscores by score **/
              highscores.sort(function(a, b) {
                  return b.attributes.score - a.attribute.score;
          });
          response.write(JSON.stringify(highscore))
          response.end();
      });
  } else {
      response.end();
  }
  } else {
      response.end();
  }
});

server.listen(4201);
