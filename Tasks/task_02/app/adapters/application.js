import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
  host: 'https://student.computing.edgehill.ac.uk/~mhall/cis3301api'
});
